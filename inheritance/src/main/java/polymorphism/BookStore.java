package polymorphism;

public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];

        // Fill the Book[]
        // 1st&3rd - use Book
        //2nd, 4th, 5th - use ElectronicBook
        books[0] = new Book("The Familiar", "Leigh Bardugo");
        books[1] = new ElectronicBook("Witness ", "Jamel Brinkley", 30);
        books[2] = new Book("Father and Son", "Jonathan Raban");
        books[3] = new ElectronicBook("All Souls", "Saskia Hamilton", 20);
        books[4] = new ElectronicBook("Hangman", "Maya Binyam", 160);

        // Loop to print all of the books.
        for (int i = 0; i < books.length; i++) {
            System.out.println("Book" + (i+1) + ": " + books[i]);
        }
    }
}
