package polymorphism;

public class ElectronicBook extends Book {
    private int numberBytes;

    // Constructor
    public ElectronicBook(String title, String author, int numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    // Getter method to get bytes in numberBytes
    public int getNumberBytes() {
        return this.numberBytes;
    }

    @Override
    public String toString() {
        String fromBase = super.toString();
        return fromBase + ", number of bytes: " + numberBytes;
    }
}
