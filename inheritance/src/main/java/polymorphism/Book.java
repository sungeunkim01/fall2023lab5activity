package polymorphism;

public class Book {
    protected String title;
    private String author;

    // Constructor
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    // Getter method for two fields
    public String getTitle() {
        return this.title;
    }
    public String getAuthor() {
        return this.author;
    }

    @Override
    public String toString() {
        return this.title + "by" + this.author;
    }

}
